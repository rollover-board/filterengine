#!/usr/bin/env python3
import pandas as pd
from numpy import array
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.preprocessing.text import one_hot
from keras.callbacks import EarlyStopping
from keras_preprocessing.sequence import pad_sequences
import keras.layers as layers

data = pd.read_csv("spam_text_message_20170820-data.csv")
msgs = data['Message']

# spam or ham indicators
categories = data['Category']

# convert textual categories to ints
for idx, label in enumerate(categories):
    if categories[idx] == "spam":
        categories[idx] = 1
    else:
        categories[idx] = 0

max_tokens = 2000
max_len = 25

categories = categories.astype(int)

# Break out distinct training & validation sets
x_train, x_test, y_train, y_test = train_test_split(msgs, categories, test_size=0.25)

# Add a vectorize layer to prep raw strings
vectorize_layer = layers.TextVectorization(output_mode="int", output_sequence_length=max_len, max_tokens=max_tokens)

# fit the vectorize layer on training data
vectorize_layer.adapt(x_train)

# Create model
model = Sequential()
model.add(layers.Input(shape=(1,), dtype="string"))
model.add(vectorize_layer)
model.add(layers.Embedding(max_tokens + 1, 128))
model.add(layers.Flatten())
model.add(layers.Dense(1, activation="sigmoid"))

model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])

# prevent overfitting with early stopping
es = EarlyStopping(monitor='accuracy', patience=4)
model.fit(x_train, y_train, epochs=50, verbose=0, callbacks=[es])

# evaluate the model
loss, accuracy = model.evaluate(x_test, y_test, verbose=0)
accuracy = accuracy*100
print('Accuracy:', accuracy )

model.save("target/filter-engine-model")
