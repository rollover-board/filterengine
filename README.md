# FilterEngine
A textual spam filtering model

# Building
You can use pip or conda to manage project dependencies.

To install a new environment and activate it using conda:
```
conda env create -f environment.yaml
conda activate rollover-filterengine
```

To update the environment after amending dependencies:
```
conda env update --file environment.yaml --prune
```
this may require deactivating and reactivating the env.

# Deploying
Create a gilab deploy token
Create a ./token file.
inside have this export ROLLOVER_DEPLOY_TOKEN=<gitlab_token>
run ./build deploy

# Data Source
https://www.kaggle.com/team-ai/spam-text-message-classification

# TODO:
Add extra text filtering for stopwords
